import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/HomePage.vue'
import AboutUs from './pages/AboutPage.vue'
import Fallkonferenz from './pages/FallkonferenzPage.vue'
import Fortbildungen from './pages/FortbildungenPage.vue'
import Literatur from './pages/LiteraturPage.vue'
import Kontakt from './pages/KontaktPage.vue'
import Impressum from './pages/ImpressumPage.vue'
import Transparenz from './pages/TransparenzPage.vue'
import Datenschutz from './pages/DatenschutzPage.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        params: true,
    },
    {
        path: '/about-us',
        name: 'AboutUs',
        component: AboutUs,
        params: true,
    },
    {
        path: '/Fallkonferenzen',
        name: 'Fallkonferenzen',
        component: Fallkonferenz,
        params: true,
    },
    {
        path: '/fortbildungen',
        name: 'Fortbildungen',
        component: Fortbildungen,
        params: true,
    },
    {
        path: '/literatur',
        name: 'Literatur',
        component: Literatur,
        params: true,
    },
    {
        path: '/contact',
        name: 'Kontakt',
        component: Kontakt,
        params: true,
    },
    {
        path: '/impressum',
        name: 'Impressum',
        component: Impressum,
        params: true,
    },
    {
        path: '/transparenz',
        name: 'Transparenz',
        component: Transparenz,
        params: true,
    },
    {
        path: '/datenschutz',
        name: 'Datenschutz',
        component: Datenschutz,
        params: true,
    },

]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router