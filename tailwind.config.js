/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Montserrat", "Sans-serif"],
      },

      textUnderlineOffset: {
        25: "25px",
      },
      textDecorationThickness: {
        5: "5px",
      },
      colors: {
        "footer-color": "#6c757d",
        "nav-color": "#495057",
        "nav-background": "#f8fafc",
        "nav-border": "#dee2e6",
        "sin-blue": "#2a69b0",
        "sin-body-blue": "#f1f6fa",
        "sin-logo-blue": "#88B7D4",
        "regal-blue": "#0a2540",
        "regal-yellow": "#DED66F",
        "black-trans-80": "rgba(0, 0, 0, 0.8)",
        "black-trans-70": "rgba(0, 0, 0, 0.7)",
        "black-trans-60": "rgba(0, 0, 0, 0.6)",
        "black-trans-50": "rgba(0, 0, 0, 0.5)",
        "black-trans-40": "rgba(0, 0, 0, 0.4)",
        "black-trans-30": "rgba(0, 0, 0, 0.3)",
        "black-trans-20": "rgba(0, 0, 0, 0.2)",
        "black-trans-10": "rgba(0, 0, 0, 0.1)",
        "header-gray": "rgba(168, 166, 161, 0.45)",
        "header-blau-400": "rgba(91, 118, 156, 0.73)",
        "header-blau-600": "rgba(57, 102, 148, 0.88)",

        "sin-body-blue": "#f1f5f9",
        "sin-white": "#fbfcfc",
        "gray-150": "#e6ebf0",
        "calendar-bg": "#F9FAFB",

        "coo-green": "#04af85",
        "coo-orange": "#da694d",
        "coo-side-nav-text": "#98a6ad",
        "coo-subheader": "#2b2b2b",
        "coo-body-bg": "#fafbfe",
        "coo-bg-gray": "#f8f8f8",
        "coo-gray": "#6c757d",
        "coo-bg-color-gray": "#6c757d",
        "coo-bg-side-nav": "#313a46",
        "coo-bg-side-nav-hover": "#3c4655",
        "coo-danger-bg": "#ff5b5b",

        "footer-color": "#2b2b2b",
        "nav-color": "#495057",
        "nav-background": "#f8fafc",
        "nav-border": "#dee2e6",
        "footer-text": "#9c9c9c",
        "footer-hover": "#ffffff",
      },
    },
  },
  plugins: [],
};
